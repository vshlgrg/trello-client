import requests
import json
from pprint import pprint


def print_card(card_json):
    card_json = card_json[0]
    print("="*10)
    print("NAME: {}".format(card_json['name']))
    print("DESCRIPTION: {}".format(card_json['description']))
    print("="*10)


def main():
    resp = requests.get("http://localhost:8000/trello/board/")
    print("Select a board")
    boards = json.loads(resp.text)
    boardmap  = {}
    for idx, board in enumerate(boards):
        boardmap[idx] = board
        print(str(idx) + ". " + board['name'])

    board_idx = input()

    print("Select a task list")
    board_uuid = boardmap[int(board_idx)]['uuid']

    resp = requests.get("http://localhost:8000/trello/board/{}/tasklist".format(board_uuid))

    tasklists = json.loads(resp.text)
    taskmap  = {}
    for idx, tasklist in enumerate(tasklists):
        taskmap[idx] = tasklist
        print(str(idx) + ". " + tasklist['name'])

    tasklist_idx = input()

    print("Select a card")
    tasklist_uuid = taskmap[int(tasklist_idx)]['uuid']

    resp = requests.get("http://localhost:8000/trello/tasklist/{}/card".format(tasklist_uuid))

    cards = json.loads(resp.text)
    cardmap  = {}
    for idx, card in enumerate(cards):
        cardmap[idx] = card
        print(str(idx) + ". " + card['name'])

    card_idx = input()

    card_uuid = cardmap[int(card_idx)]['uuid']

    resp = requests.get("http://localhost:8000/trello/card/{}".format(card_uuid))
    print_card(json.loads(resp.text))


main()
